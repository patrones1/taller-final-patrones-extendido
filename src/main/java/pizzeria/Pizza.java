package pizzeria;

public class Pizza {

    private final boolean conExtraQueso;
    private final boolean conPepino;
    private final boolean conJamon;
    private final boolean conPinna;
    private final boolean conBocadillo;

    private Pizza(Builder builder) {

        this.conExtraQueso = builder.conExtraQueso;
        this.conPepino = builder.conPepino;
        this.conJamon = builder.conJamon;
        this.conPinna = builder.conPinna;
        this.conBocadillo = builder.conBocadillo;

    }

    public void darDescripcionPizza() {

        StringBuilder descripcionPizza = new StringBuilder("Pizza con base de queso");

        if (conExtraQueso) {

            descripcionPizza.append(" - con extra queso");

        }

        if (conPepino) {

            descripcionPizza.append(" - con pepino");

        }

        if (conJamon) {

            descripcionPizza.append(" - con jamon");

        }

        if (conPinna) {

            descripcionPizza.append(" - con pinna");

        }

        if (conBocadillo) {

            descripcionPizza.append(" - con bocadillo");

        }

        System.out.println(descripcionPizza);

    }


    public static class Builder {

        private boolean conExtraQueso;
        private boolean conPepino;
        private boolean conJamon;
        private boolean conPinna;
        private boolean conBocadillo;

        public Builder() {
        }

        public Builder agregarExtraQueso() {

            conExtraQueso = true;
            return this;

        }

        public Builder agregarPepino() {

            conPepino = true;
            return this;

        }

        public Builder agregarJamon() {

            conJamon = true;
            return this;

        }

        public Builder agregarPinna() {

            conPinna = true;
            return this;

        }

        public Builder agregarBocadillo() {

            conBocadillo = true;
            return this;

        }

        public Pizza build() {

            return new Pizza(this);

        }

    }

}
