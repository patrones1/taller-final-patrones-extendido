package pizzeria;

import java.util.ArrayList;
import java.util.List;

public class Cliente implements Observable {

    private final String nombreCliente;
    private int puntosCliente;
    private final List<Observador> observadores;

    public Cliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
        this.puntosCliente = 0;
        this.observadores = new ArrayList<>();
    }

    public void pedirPizza(Pizza pizza) {

        if (pizza != null){

            acumularPuntos();

            notificarObservadores();

        }

    }

    private void acumularPuntos() {

        this.puntosCliente += 100;

    }

    public int darPuntosAcumulados() {

        return this.puntosCliente;

    }

    public String darNombreCliente() {

        return this.nombreCliente;

    }

    @Override
    public void agregarObservador(Observador observador) {

        observadores.add(observador);

    }

    @Override
    public void eliminarObservador(Observador observador) {

        observadores.remove(observador);

    }

    @Override
    public void notificarObservadores() {

        for (Observador observer : observadores) {

            observer.actualizar(this);

        }

    }

}
