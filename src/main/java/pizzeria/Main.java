package pizzeria;

public class Main {
    public static void main(String[] args) {

        Cliente cliente001 = new Cliente("Juan");
        Cliente cliente002 = new Cliente("Maria");

        PuntosObservador observador = new PuntosObservador();

        cliente001.agregarObservador(observador);
        cliente002.agregarObservador(observador);

        Pizza pizza001 = new Pizza.Builder()
                .agregarExtraQueso()
                .agregarPepino()
                .agregarJamon()
                .agregarPinna()
                .agregarBocadillo()
                .build();

        pizza001.darDescripcionPizza();

        cliente001.pedirPizza(pizza001);

        Pizza pizza002 = new Pizza.Builder()
                .build();

        pizza002.darDescripcionPizza();

        cliente001.pedirPizza(pizza002);

        Pizza pizza003 = new Pizza.Builder()
                .agregarPinna()
                .build();

        pizza003.darDescripcionPizza();

        cliente002.pedirPizza(pizza003);

        Pizza pizza004 = new Pizza.Builder()
                .agregarBocadillo()
                .agregarExtraQueso()
                .build();

        pizza004.darDescripcionPizza();

        cliente002.pedirPizza(pizza004);

        Pizza pizza005 = new Pizza.Builder()
                .agregarPepino()
                .agregarJamon()
                .agregarPinna()
                .build();

        pizza005.darDescripcionPizza();

        cliente002.pedirPizza(pizza005);

        cliente001.notificarObservadores();
        cliente002.notificarObservadores();

        cliente002.eliminarObservador(observador);

        cliente001.notificarObservadores();
        cliente002.notificarObservadores();

    }

}
