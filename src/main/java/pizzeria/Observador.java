package pizzeria;

public interface Observador {

    void actualizar(Observable observable);

}
