package pizzeria;

public class PuntosObservador implements Observador {

    @Override
    public void actualizar(Observable observable) {

        if (observable instanceof Cliente) {

            Cliente cliente = (Cliente) observable;

            System.out.println("Puntos actualizados para el cliente " + cliente.darNombreCliente() + ": " + cliente.darPuntosAcumulados());

        }

    }

}
